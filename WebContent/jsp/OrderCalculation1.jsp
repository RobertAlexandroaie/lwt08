<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Order Calculation 1</title>
</head>
<body>
	<CENTER>
		<TABLE BORDER=5>
			<TR>
				<TH CLASS="TITLE">Parametri cerere folosind jsp:setProperty</TH>
		</TABLE>
		<jsp:useBean id="order" class="lwt.lab.beans.Order" />
		<jsp:setProperty name="order" property="productName"
			value='<%=request.getParameter("productName")%>' />
		<%
		    short quantity = 0;
		    try {
				quantity = Short.parseShort(request.getParameter("quantity"));
		    } catch (NumberFormatException nfe) {
				nfe.printStackTrace();
		    }
		%>
		<jsp:setProperty name="order" property="quantity"
			value="<%=quantity%>" />
		<%
		    double price = 1.0;
		    try {
				String priceString = request.getParameter("price");
				price = Double.parseDouble(priceString);
		    } catch (NumberFormatException nfe) {
		    }
		%>
		<jsp:setProperty name="order" property="price" value="<%=price%>" />
		<BR>

		<%
		    double totalValue = price * quantity;
		%>
		<jsp:setProperty name="order" property="totalValue"
			value="<%=totalValue%>" />
		<TABLE BORDER=1>
			<TR CLASS="COLORED">
				<TH>Articol
				<TH>Cantitate
				<TH>Pret
				<TH>Total Valoare
			<TR ALIGN="RIGHT">
				<TD><jsp:getProperty name="order" property="productName" />
				<TD><jsp:getProperty name="order" property="quantity" />
				<TD>RON <jsp:getProperty name="order" property="price" />
				<TD>RON <jsp:getProperty name="order" property="totalValue" />
		</TABLE>
	</CENTER>
</body>
</html>