<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<CENTER>
		<TABLE BORDER=5>
			<TR>
				<TH CLASS="TITLE">Parametri cerere folosind valori implicite</TH>
		</TABLE>
		<jsp:useBean id="order" class="lwt.lab.beans.Order" />
		<jsp:setProperty name="order" property="productName" param=”productName” />
		<jsp:setProperty name="order" property="quantity" param="quantity" />
		<jsp:setProperty name="order" property="price" param="price" />
		<%
		    double totalValue = Double.parseDouble(order.getPrice()) * Double.parseDouble(order.getQuantity());
		%>
		<jsp:setProperty name="order" property="totalValue"
			value="<%=totalValue%>" />
		<TABLE BORDER=1>
			<TR CLASS="COLORED">
				<TH>Articol
				<TH>Cantitate
				<TH>Pret
				<TH>Total Valoare
			<TR ALIGN="RIGHT">
				<TD><jsp:getProperty name="order" property="productName" />
				<TD><jsp:getProperty name="order" property="quantity" />
				<TD>RON <jsp:getProperty name="order" property="price" />
				<TD>RON <jsp:getProperty name="order" property="totalValue" />
		</TABLE>
	</CENTER>
</body>
</html>