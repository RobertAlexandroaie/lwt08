<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Simple</title>
</head>
<body>
	<TABLE BORDER=5 ALIGN="CENTER">
		<TR>
			<TH CLASS="TITLE">Folosirea JavaBeans cu JSP</TH>
		</TR>
	</TABLE>
	<jsp:useBean id="stringBean" class="lwt.lab.beans.SimpleBean" />
	<OL>
		<LI>Valoare initiala (jsp:getProperty): <I><jsp:getProperty
					name="stringBean" property="message" /></I></LI>
		<LI>Valoarea initiala (expresie JSP): <I><%=stringBean.getMessage()%></I></LI>
		<LI><jsp:setProperty name="stringBean" property="message"
				value="Mesaj setat cu jsp:setProperty" /> Valoarea dupa setarea
			proprietatii cu jsp:setProperty: <I><jsp:getProperty
					name="stringBean" property="message" /></I></LI>
		<LI>
			<%
			    stringBean.setMessage("Mesaj setat cu bean.setMessage()");
			%> Valoarea dupa setarea proprietatii cu scriptlet: <I><%=stringBean.getMessage()%></I>
		</LI>
	</OL>
</body>
</html>