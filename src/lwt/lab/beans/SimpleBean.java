/**
 * 
 */
package lwt.lab.beans;

/**
 * @author Robert
 *
 */
public class SimpleBean {
    private String message = "Mesaj predefinit";
    private Boolean flag;

    /**
     * @return the message
     */
    public String getMessage() {
	return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
	this.message = message;
    }

    /**
     * @return the flag
     */
    public Boolean getFlag() {
	return flag;
    }

    /**
     * @param flag
     *            the flag to set
     */
    public void setFlag(Boolean flag) {
	this.flag = flag;
    }

}
